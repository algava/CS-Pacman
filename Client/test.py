import pygame
import time

def i2b(int_data):
    return int_data.to_bytes(8, 'big')

def b2i(bytes_data):
    return int.from_bytes(bytes_data, 'big')

def s2b(string_data):
    return string_data.encode('ascii')

def b2s(bytes_data):
    return bytes_data.decode('ascii')

class Player(pygame.sprite.Sprite):

    def __init__(self, x, y):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.Surface([50, 50])
        self.image.fill([0, 255, 127])
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y
        self.pos = [self.rect.x, self.rect.y]
        self.mov = [[0, 0], [0,0]]


    def update(self):
        self.pos[0] += self.mov[0][0]
        self.pos[1] += self.mov[0][1]
        self.mov.append(self.mov.pop())
        self.rect.x = self.pos[0]
        self.rect.y = self.pos[1]

class Wall(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.Surface([50, 50])
        self.image.fill([0, 0, 255])
        self.rect = self.image.get_rect()
        self.rect.x = 100
        self.rect.y = 100


if __name__ == '__main__':
    pygame.init()
    display = pygame.display.set_mode([400, 400])

    reloj = pygame.time.Clock()

    end = False
    p = Player(150, 150)
    ps = Player(150, 150)
    ps.image = pygame.transform.scale(p.image, (32, 32))
    ps.rect = ps.image.get_rect()
    p = ps
    

    jugador = pygame.sprite.Group()
    muro = pygame.sprite.Group()
    todos = pygame.sprite.Group()

    m = [[1,1,1,1,1,1,1,1],
         [1,0,0,0,0,0,0,1],
         [1,0,1,1,1,1,0,1],
         [1,0,1,0,0,0,0,1],
         [1,0,1,0,1,1,0,1],
         [1,0,1,0,1,1,0,1],
         [1,0,0,0,0,0,0,1],
         [1,1,1,1,1,1,1,1]]

    jugador.add(p)
    todos.add(p)

    for y in range(0, 8):
        for x in range(0, 8):
            if m[x][y]: 
                w = Wall()  
                w.rect.x = x *50
                w.rect.y = y * 50       
                muro.add(w)
                todos.add(w)
    while not end:



        for event in pygame.event.get():

            if event.type == pygame.QUIT:
                end = True

            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_UP:
                    #p.mov.pop()
                    p.mov.append([0, -5])

                elif event.key == pygame.K_DOWN:
                    #p.mov.pop()
                    p.mov.append([0, 5])


                elif event.key == pygame.K_LEFT:
                    #p.mov.pop()
                    p.mov.append([-5, 0])


                elif event.key == pygame.K_RIGHT:
                    #p.mov.pop()
                    p.mov.append([5, 0])




        #elif not ls1 and ls2:



        a = p
        a.rect.x += p.mov[1][0]
        a.rect.y += p.mov[1][1]

        ls = pygame.sprite.spritecollide(a, muro, False)
        if ls:
            p.mov.pop()
            p.mov.append([0, 0])

        

        '''
        print(p.mov, p.mov2)
        if p.mov2 != [0,0]:
            p1 = p
            p1.rect.x += p.mov2[0]
            p1.rect.y += p.mov2[1]

            l1 = pygame.sprite.spritecollide(p1, muro, False)
            if not l1:
                p.mov = p.mov2
                p.mov2 = [0,0]
        '''

        #print(p.mov, p.mov2)
        print('\n')
        display.fill([0, 0, 0])
        todos.update()
        todos.draw(display)
        pygame.display.flip()
        reloj.tick(50)

