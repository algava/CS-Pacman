import pygame
import time
import json

class SpriteSheet():

    def __init__(self, x_size, y_size, file_name = "Pac-Man Smooth.png"):
        self.sheet = pygame.image.load(file_name)
        self.x_size = x_size
        self.y_size = y_size

    def cropSprite(self, pos):
        return self.sheet.subsurface(pos[0]*self.x_size, pos[1]*self.y_size, self.x_size, self.y_size)

    def cropSprites(self, pos_i, pos_f):
        list_of_sprites = []
        for y in range(pos_i[1], pos_f[1]+1):

            for x in range(pos_i[0], pos_f[0]+1):
                print(x, y)
                list_of_sprites.append(self.cropSprite([x, y]))

        return list_of_sprites

class Player(pygame.sprite.Sprite):

    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load("Pac-Man Smooth.png")
        self.image = self.image.subsurface(0, 0, 50, 50)
        #self.image=pygame.Surface([40,40])
        #self.image.fill([0, 255, 0])
        self.rect=self.image.get_rect()
        


class Pacman():

    def __init__(self):
        pygame.init()
        self.display = pygame.display.set_mode([1260, 1260])
        self.spriteSheet = SpriteSheet(50, 50)

    def run(self):
        print('Jugando...')
        #self.display.blit(self.spriteSheet.cropSprite([17, 2]), [0,0])
        a = self.spriteSheet.cropSprites([17, 0], [17, 11])
        game_over = False
        clock = pygame.time.Clock()
        i = 0
        img = pygame.image.load("test1.png")
        x1 = 100
        y1 = 100
        v = [0, 0]
        ab = Player()
        players = pygame.sprite.Group()
        players.add(ab)
        print(ab.rect)
        while not game_over:

            for event in pygame.event.get():

                if event.type == pygame.QUIT:
                    game_over = True

                elif event.type == pygame.KEYDOWN:
                    
                    if event.key == pygame.K_UP:
                        print("up")
                        v = [0, -5] 

                    elif event.key == pygame.K_DOWN:
                        print("down")

                        v = [0, 5] 

                    elif event.key == pygame.K_LEFT:
                        print("left")
                        v = [-5, 0] 

                    elif event.key == pygame.K_RIGHT:
                        print("right")
                        v = [5, 0]

                    elif event.key == pygame.K_SPACE:
                        i+=1
            self.display.fill([0, 0, 0])
            self.display.blit(img,[0,0])
            ab.rect.x += v[0]
            ab.rect.y += v[1]
            players.draw(self.display)
            i+=1
            if i == 11*10:
                i=0
            x1 += v[0]
            y1 += v[1]
            self.display.blit(a[i//10], [x1, y1])
            pygame.display.flip()
            clock.tick(50)





if __name__ == "__main__":
    pacman = Pacman()
    pacman.run()